/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Reponses;
import static entities.Reponses_.isBonneReponse;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author kabbaj
 */
@Stateless
public class ReponsesFacade extends AbstractFacade<Reponses> {

    @PersistenceContext(unitName = "Examen-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReponsesFacade() {
        super(Reponses.class);
    }
    
    public boolean Correction(String Reponses )
    {
         try{
             
            Reponses reponses = (Reponses ) em.createNamedQuery("Reponses .Correction",Reponses.class).setParameter("IsBonneReponse",isBonneReponse).getSingleResult();
            if(reponses.getIsBonneReponse()!= 0) return true ;
            else   return false ;
        }catch(Exception e){
            return false ;
        }
    }

    public boolean Valider(String reponse , String idQuestion)
    {
         try{
            Reponses reponses = (Reponses) em.createNamedQuery("Reponses.Valider",Reponses.class).setParameter("reponse",reponse).setParameter("idQuestion",idQuestion).getSingleResult();
            if(reponses!= null) return true ;
            else   return false ;
        }catch(Exception e){
            return false ;
        }
    }

    public List<Reponses> findby(int id) {
    Query query = em.createQuery("SELECT f FROM Reponses f WHERE idQuestion = id");
        
        return (List<Reponses>) (Reponses) query.getSingleResult();
              
    }

    

    
}
