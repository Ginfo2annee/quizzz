package entities;

import entities.Notes;
import entities.Questions;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-06-07T11:44:57")
@StaticMetamodel(Examens.class)
public class Examens_ { 

    public static volatile ListAttribute<Examens, Notes> notesList;
    public static volatile SingularAttribute<Examens, Integer> nombreQuestions;
    public static volatile SingularAttribute<Examens, Integer> idExamen;
    public static volatile ListAttribute<Examens, Questions> questionsList;
    public static volatile SingularAttribute<Examens, String> nom;
    public static volatile SingularAttribute<Examens, String> matiere;

}