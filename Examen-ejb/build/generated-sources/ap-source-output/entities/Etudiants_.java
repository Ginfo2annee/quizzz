package entities;

import entities.Notes;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-06-07T11:44:57")
@StaticMetamodel(Etudiants.class)
public class Etudiants_ { 

    public static volatile ListAttribute<Etudiants, Notes> notesList;
    public static volatile SingularAttribute<Etudiants, Double> note;
    public static volatile SingularAttribute<Etudiants, String> password;
    public static volatile SingularAttribute<Etudiants, Integer> matricule;
    public static volatile SingularAttribute<Etudiants, String> nom;
    public static volatile SingularAttribute<Etudiants, String> prenom;
    public static volatile SingularAttribute<Etudiants, String> email;

}