package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-06-07T11:44:57")
@StaticMetamodel(Reponses.class)
public class Reponses_ { 

    public static volatile SingularAttribute<Reponses, String> reponse;
    public static volatile SingularAttribute<Reponses, Integer> idQuestion;
    public static volatile SingularAttribute<Reponses, Short> isBonneReponse;
    public static volatile SingularAttribute<Reponses, Integer> idReponse;

}