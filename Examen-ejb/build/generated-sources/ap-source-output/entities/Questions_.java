package entities;

import entities.Examens;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2017-06-07T11:44:57")
@StaticMetamodel(Questions.class)
public class Questions_ { 

    public static volatile SingularAttribute<Questions, Examens> idExamen;
    public static volatile SingularAttribute<Questions, String> question;
    public static volatile SingularAttribute<Questions, String> consigne;
    public static volatile SingularAttribute<Questions, Integer> idQuestions;

}