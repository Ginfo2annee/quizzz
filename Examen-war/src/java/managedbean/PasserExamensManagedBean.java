/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbean;

import entities.Examens;
import entities.Questions;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import sessions.ExamensFacade;

/**
 *
 * @author Hajar MDD
 */
@Named(value = "passerExamensManagedBean")
@SessionScoped
public class PasserExamensManagedBean implements Serializable {
@EJB
private ExamensFacade examensfacade ; 
private int examenId ; 
private Examens examens ; 
public List<Questions> getQuestions() {
return examens.getQuestionsList() ; 
}
    public PasserExamensManagedBean() {
    }
 public int getExamenId() {
 return examenId ; 
 
 }   
 public void setExamensId(int examenId){
  this.examenId=examenId ; 
 }
 public void loadExamens(){
 examens = examensfacade.find(examenId) ; 
 }
 
 
}
