/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import entities.Questions;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import sessions.QuestionsFacade;


@Named(value = "reponsesquestionManagedBean")
@SessionScoped
public class ReponsesquestionManagedBean implements Serializable {

    /**
     * Creates a new instance of ReponsesquestionManagedBean
     */
    
    private int idQuestions ; 
    private Questions question ; 
    @EJB
    
    private QuestionsFacade questionFacade ; 
      
public ReponsesquestionManagedBean() {
    }

    public int getIdQuestions() {
        return idQuestions;
    }

    public void setIdQuestions(int idQuestions) {
        this.idQuestions = idQuestions;
    }
    
    public Questions getDetails() {
    return question ; 
    }
   
    
    
    
}
